import React,{Component} from 'react';

class Login extends Component{

getUserDetails= (e)=>{

    const usr=e.target.elements.uname.value;
    const pwd=e.target.elements.pass.value;
    e.preventDefault();
    
}
    render(){
        return(
            <div className="container">
  <div className="form-box">
    <h2 className="mb-5">Login Here</h2>
    <form className="form" onSubmit={this.getUserDetails}>
      <input type="text" name="uname" className="form-control" placeholder="Username" />
      <input type="password" name="pass" className="form-control" placeholder="Password" />
     
      <button type="submit" className="btn btn-primary btn-block mt-4">Submit</button>
    </form>
  </div>
</div>

        );
    }
}

export default Login;