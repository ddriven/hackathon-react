import React,{Component} from 'react';
import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';
import tagdetail from '../tagdetail.json';
let options;

  
class Highchart extends Component {
    
    state={
        type:"line"
    }
    chartType=(e)=>{

    const chartType=e.target.innerHTML;
        this.setState({type:chartType});
        console.log(chartType);
    }

    render(){
        console.log(this.state.type);
        options = {
            chart:{
                type: this.state.type
            },
            xAxis:{
                type: "datetime",
                minPadding: 0,
                maxPadding: 0,
                labels: {
                    format: '{value:%Y-%b-%e}'
                  }
            },
            yAxis: {
                title: {
                  text: 'Value'
                }
              },
            title: {
              text: 'Trender'
            },
            series: [{
                name:"XIA154.PV",
                data: tagdetail
            }]
          }
        
        return(
<div>
        <div className="btn-group">
  <button type="button" className="btn btn-primary"  onClick={this.chartType}>line</button>
  <button type="button" className="btn btn-primary"  onClick={this.chartType}>bar</button>
  <button type="button" className="btn btn-primary"  onClick={this.chartType}>area</button>
</div>
   <HighchartsReact
  highcharts={Highcharts}
  constructorType={'chart'}
  options={options}
/>
</div>

        );
    }
}

    


export default Highchart;