import React from 'react';
import Clock from 'react-live-clock';

const time="";

const Header =()=>(
  
    <header>
     
      <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a className="navbar-brand" href="javascript:void();">Header</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
     
        <span className="navbar-brand ml-auto">
        <Clock
        format={' DD-MM-YYYY HH:mm:ss'}
        ticking={true}
        timezone={'India/kolkatta'} />
            </span>
        
      </nav>
    </header>
)

export default Header;