import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import { Grid, GridColumn as Column, GridCell } from '@progress/kendo-react-grid';
import tags from './tag.json';
import '@progress/kendo-theme-default/dist/all.css';
import Highchart from './components/Highchart';
import Modal from 'react-responsive-modal';


class App extends Component {
  constructor(props) {
    super(props);
    console.log(tags.TagData);
    this.state = this.createState(0, 3);
    this.pageChange = this.pageChange.bind(this);
    this.updatePagerState = this.updatePagerState.bind(this);
  }

  pageChange(event) {
    this.setState(this.createState(event.page.skip, event.page.take));
  }


  saveAndClose()
  {
    this.setState({ open: false });
  }

  onOpenModal = () => {
    this.setState({
      showModal: true
  });
  };
 
  onCloseModal = () => {
    this.setState({
      showModal: false
  });
  };

  createState(skip, take) {
    return {
      items: tags.TagData.slice(skip, skip + take),
      total: tags.TagData.length,
      skip: skip,
      pageSize: take,
      pageable: this.state ? this.state.pageable : {
        buttonCount: 3,
        info: true,
        type: 'numeric',
        pageSizes: true,
        previousNext: true
      },
      showModal:false
    };
  }
  close(){
    
  
  }

  open(){
  
   // let showModal = Object.assign({}, this.state.showModal);    //creating copy of object
   // showModal.showModal = true;                     //updating value
   // this.setState({showModal});
    //console.log(this.state.showModal);
    // const showModal = Object.assign({}, this.state.showModal, true);
    // console.log(showModal)
     //this.setState(Object.assign({}, this.state, { showModal: showModal }));
    // console.log(this.state.showModal)
    //this.setState(Object.assign({}, this.state, { showModal: true }));
    
  }

  updatePagerState(key, value) {
    const newPageableState = Object.assign({}, this.state.showModal, { [key]: value });
    this.setState(Object.assign({}, this.state, { pageable: newPageableState,showModal: false }));
    
  }

  renderTrender() {

  }
  rowClick(event) {
    console.log(this.state);
    this.setState({
      showModal: true
  });
    console.log(this.state);
  };



  render() {
   
    
    return (
      <div className="App">
        <Header />
        <div className="container">
          <div className="row mt-5">
            <div className="col-lg-12">
              <div className="card mt-4">
                <div className="card-body">
                  <Grid
                    style={{ height: '400px' }}
                    data={this.state.items}
                    onPageChange={this.pageChange}
                    total={this.state.total}
                    skip={this.state.skip}
                    pageable={this.state.pageable}
                    pageSize={this.state.pageSize}
                    onRowClick={this.rowClick.bind(this)}
                  >
                    <Column field="name" title="Name" />
                    <Column field="description" title="Description" />
                    <Column field="unit" title="Unit" />
                    <Column field="source" title="Source" />
                  </Grid>
                  { this.state.showModal ?  <div>
          <Highchart/>
                 </div> : null }
                 
                 <div>
      
       
      </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        <Modal open={this.state.showModal} onClose={this.onCloseModal} center>
        <Highchart/>
        </Modal>
      </div>
    );
  }
}

export default App;
